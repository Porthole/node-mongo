FROM node:15

WORKDIR /opt/app
COPY . .

RUN npm i
EXPOSE 3000

CMD npm start
