Node mongo exemple
==================

To run the server : 
<pre>
npm install
npm start

Env needed : 
- DB_NAME (no default)

Env not mandatory : 
- DB_PORT (default 27017)
- DB_HOST (default localhost)
- EXPRESS_PORT (default 3000)
</pre>

Healthcheck route*: localhost:3000/api/healthcheck

route* : It depends on the port you choseocker s
